package upm.bd

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.regression.{LinearRegression, LinearRegressionModel}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.ml.Pipeline


object Modeller {

  def main(args: Array[String]) {

    // Verificar el archivo CSV de entrada
    if (args.length == 0) {
      println("ERROR: You need to input a file as parameter")
    } else {

      // Archivo a analizar
      val file = args(0)

      //Fijar los Logs de Spark -> Mostrar unicamente información relevante
      Logger.getLogger("org").setLevel(Level.ERROR)
      Logger.getLogger("akka").setLevel(Level.ERROR)

      /* INICIAR SESIÓN DE SPARK */
      val spark = SparkSession
        .builder
        .master("local[2]")
        .appName("Big Data P1")
        .config("spark.sql.warehouse.dir", "file://") // Spark Warehouse en el directorio local
        .getOrCreate()
      import spark.implicits._
      //import spark.sql

      println("Spark Session started\n")

      /* PROGRAMA PRINCIPAL DE SPARK */
      try {

        /* DATOS DE ENTRADA */

        // Se requiere introducir como argumento la ruta de un archivo CSV para analizar
        // Este archivo se encuentra disponible en http://stat-computing.org/dataexpo/2009/the-data.html

        //Cargar CSV en un DF -> Se identifican las cabeceras y se infieren los tipos de datos
        val loadedDF = spark.read
          .format("csv")
          .option("header", "true")
          .option("inferSchema", "true")
          .option("nullValue", "NA")
          .load(file)

        /* DATA FRAME DE DATOS */

        //Lista de columnas a eliminar (prohibidas)
        val delete_cols = List("ArrTime", "ActualElapsedTime", "AirTime", "TaxiIn", "Diverted", "CarrierDelay", "WeatherDelay", "NASDelay", "SecurityDelay", "LateAircraftDelay")

        //Lista de columnas a eliminar (no se consideran utiles)
        val useless_rows = List("Cancelled", "CancellationCode", "FlightNum", "TailNum")

        //Lista de columnas válidas
        val useful_rows = loadedDF.columns.filter(nombre => !delete_cols.contains(nombre) && !useless_rows.contains(nombre))


        val DF = loadedDF.select(useful_rows.map(col): _*) //cargamos el dataset con los 100 primeros registros para que no se sobrecargue la memoria
          .na.drop // Limpieza de los datos (se eliminan filas con valores nulos
          .withColumn("TaxiOutDelay", expr("TaxiOut - 16")) //retardo de taxiOut con respecto a la media general prevista de tiempo de taxiOut
          .withColumn("AirportDelay", expr("DepDelay + TaxiOutDelay")) //suma del retardo de salida y del retardo de taxiout
          .withColumn("CRSDepTimeF", expr("(CRSDepTime-CRSDepTime % 100)/100*60+(CRSDepTime % 100)")) //Tiempo estimado de salida (minutos)
          .withColumn("DepTimeF", expr("(DepTime-DepTime % 100)/100*60+(DepTime % 100)")) //Tiempo real de salida (minutos)
          .withColumn("CRSArrTimeF", expr("(CRSArrTime-CRSArrTime % 100)/100*60+(CRSArrTime % 100)")) //Tiempo estimado de llegada (minutos)


        println("SCHEMA AND CONTENTS OF FLIGHT DATASET (20 rows)\n")

        //Mostrar las columnas y tipos de datos
        DF.printSchema()

        //Mostrar el contenido (20 filas)
        DF.take(20).foreach(println)

        /* ANALIZAR EL DF */

        val split = DF.randomSplit(Array(0.7, 0.3)) //Se utiliza 70% para entrenamiento y 30% de evaluación
        val training = split(0)
        val test = split(1)

       /* println("MAIN CORRELATIONS\n")
        //coeficientes de correlacion entre la variable a predecir y las mas importantes
        DF.select(corr($"ArrDelay", $"AirportDelay")).show() //0.95
        DF.select(corr($"ArrDelay", $"DepDelay")).show() //0.93
        DF.select(corr($"ArrDelay", $"TaxioutDelay")).show() //0.32
        DF.select(corr($"ArrDelay", $"TaxiOut")).show() //0.32
        DF.select(corr($"ArrDelay", $"DepTimeF")).show() //0.177
        DF.select(corr($"ArrDelay", $"CRSDepTimeF")).show() //0.117
        DF.select(corr($"ArrDelay", $"CRSArrTimeF")).show() //0.113
        DF.select(corr($"ArrDelay", $"Month")).show() //-0.04
        DF.select(corr($"ArrDelay", $"CRSElapsedTime")).show() //0.012
        DF.select(corr($"ArrDelay", $"DayOfWeek")).show() //0.01
        DF.select(corr($"ArrDelay", $"Distance")).show() //0.0065
        DF.select(corr($"ArrDelay", $"DayofMonth")).show() //0.0016
        */

        //VECTOR DE COMPONENTES USADOS PARA REGRESION
        val assembler = new VectorAssembler()
          .setInputCols(Array("AirportDelay", "DepDelay", "TaxiOut", "Month", "CRSElapsedTime", "DayOfWeek", "Distance", "CRSDepTimeF", "DepTimeF", "CRSArrTimeF"))
          .setOutputCol("features")

        // MODELO ML: regresion lineal
        val lr = new LinearRegression()
          .setFeaturesCol("features") // Componentes
          .setLabelCol("ArrDelay") // Variable objetivo a predecir
          .setMaxIter(10)
          .setElasticNetParam(0.8)

        // PIPELINE DE OPERACIONES A EJECUTAR
        val pipeline = new Pipeline()
          .setStages(Array(assembler, lr))

        // Generar el modelo
        val model = pipeline.fit(training)

        //guardar o sobreescribir el modelo a disco
        model.write.overwrite.save("FlightModel")

        println("Model generated and saved in disk\n")

        //Datos del modelo de ML
        val lrModel = model.stages(1).asInstanceOf[LinearRegressionModel]

        // Datos del modelo generado
        println("LINEAR REGRESSION MODEL\n")

        println(s"Coefficients: ${lrModel.coefficients}")
        println(s"Intercept: ${lrModel.intercept}")

        val trainingSummary = lrModel.summary
        println(s"numIterations: ${trainingSummary.totalIterations}")
        println(s"objectiveHistory: ${trainingSummary.objectiveHistory.toList}")
        trainingSummary.residuals.show()

        // Datos sobre la calidad del ajuste
        println(s"RMSE: ${trainingSummary.rootMeanSquaredError}")
        println(s"r2: ${trainingSummary.r2}")

        // VALIDACIÓN CRUZADA

        // Aplicar el modelo al Dataset de test
        val result = model.transform(test)

        //Comparar la predicción con el valor real (retraso en minutos)
        result.select("prediction", "ArrDelay").show

        //RMSE = sqrt(sum((r1-r2)^2)/n)
        val suma_e2 = result.select(sum(pow($"prediction" - $"ArrDelay", 2))).first.getDouble(0)
        val n = result.count
        val RMSE = Math.sqrt(suma_e2 / n)

        // RMSE - Comparación predición y resultado real
        println("\nTest Validation\nRMSE :" + RMSE)


      } catch {
        case e: Exception =>
          println("ERROR: Exception during program execution (+" + e.getMessage + ")")
      } finally {
        // En cualquier caso se cerrará la sesión de Spark abierta
        spark.stop
        println("Spark Session finished\n")
      }
    }
  }
}