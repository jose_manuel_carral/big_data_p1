package upm.bd

import java.awt.event.{ActionEvent, ActionListener, WindowAdapter, WindowEvent}
import javax.swing._

import org.apache.spark.sql.SparkSession
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.ml.PipelineModel


object Predictor {

  def main(args: Array[String]) {

    //Fijar los Logs de Spark -> Mostrar unicamente información relevante
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)

    /* INICIAR SESIÓN DE SPARK */
    val spark = SparkSession
      .builder
      .master("local[2]")
      .appName("Big Data P1")
      .config("spark.sql.warehouse.dir", "file://") // Spark Warehouse en el directorio local
      .getOrCreate()

    println("Spark Session started\n")

    try {
      //recuperar el modelo de disco
      val model = PipelineModel.load("FlightModel")

      println("Model restored from disk\n")

      //creacion de toda la IU
      val frame = new JFrame("Arrival Delay Predictor")
      frame.setBounds(100, 100, 730, 420)
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
      frame.getContentPane.setLayout(null)

      val lblCRSDepTime = new JLabel("Scheduled Departure Time (hhmm)")
      lblCRSDepTime.setBounds(65, 30, 200, 14)
      frame.getContentPane.add(lblCRSDepTime)

      val txfCRSDepTime = new JTextField()
      txfCRSDepTime.setBounds(270, 30, 86, 20)
      frame.getContentPane.add(txfCRSDepTime)
      txfCRSDepTime.setColumns(10)

      val lblDepTime = new JLabel("Actual Departure Time (hhmm)")
      lblDepTime.setBounds(65, 60, 200, 14)
      frame.getContentPane.add(lblDepTime)

      val txfDepTime = new JTextField()
      txfDepTime.setBounds(270, 60, 86, 20)
      frame.getContentPane.add(txfDepTime)
      txfDepTime.setColumns(10)

      val CRSArrivalTime = new JLabel("Scheduled Arrival Time (hhmm)")
      CRSArrivalTime.setBounds(65, 90, 200, 14)
      frame.getContentPane.add(CRSArrivalTime)

      val txfArrivalTime = new JTextField()
      txfArrivalTime.setBounds(270, 90, 86, 20)
      frame.getContentPane.add(txfArrivalTime)
      txfArrivalTime.setColumns(10)

      val lblTaxiOut = new JLabel("Taxi Out (minutes)")
      lblTaxiOut.setBounds(65, 120, 200, 14)
      frame.getContentPane.add(lblTaxiOut)

      val txfTaxiOut = new JTextField()
      txfTaxiOut.setBounds(270, 120, 86, 20)
      frame.getContentPane.add(txfTaxiOut)
      txfTaxiOut.setColumns(10)

      val lblDistance = new JLabel("Distance (milles)")
      lblDistance.setBounds(65, 150, 200, 14)
      frame.getContentPane.add(lblDistance)

      val txfDistance = new JTextField()
      txfDistance.setBounds(270, 150, 86, 20)
      frame.getContentPane.add(txfDistance)
      txfDistance.setColumns(10)

      val lblMonth = new JLabel("Month (1-12)")
      lblMonth.setBounds(65, 180, 200, 14)
      frame.getContentPane.add(lblMonth)

      val txfMonth = new JTextField()
      txfMonth.setBounds(270, 180, 86, 20)
      frame.getContentPane.add(txfMonth)
      txfMonth.setColumns(10)

      val lblDayOfWeek = new JLabel("Day Of Week (1-5)")
      lblDayOfWeek.setBounds(65, 210, 200, 14)
      frame.getContentPane.add(lblDayOfWeek)

      val txfDayOfWeek = new JTextField()
      txfDayOfWeek.setBounds(270, 210, 86, 20)
      frame.getContentPane.add(txfDayOfWeek)
      txfDayOfWeek.setColumns(10)

      val lblArrivalDelay = new JLabel("Estimated Arrival Delay (minutes)")
      lblArrivalDelay.setBounds(400, 120, 200, 14)
      frame.getContentPane.add(lblArrivalDelay)

      val txfArrivalDelay = new JTextField()
      txfArrivalDelay.setBounds(600, 120, 86, 20)
      txfArrivalDelay.setEditable(false)
      frame.getContentPane.add(txfArrivalDelay)
      txfArrivalDelay.setColumns(10)

      val btnClear = new JButton("Clear")

      btnClear.setBounds(65, 300, 80, 20)
      frame.getContentPane.add(btnClear)

      val btnSubmit = new JButton("Predict!")

      btnSubmit.setBounds(312, 320, 100, 30)
      frame.getContentPane.add(btnSubmit)

      btnSubmit.addActionListener(new ActionListener() {
        override def actionPerformed(e: ActionEvent): Unit = {
          if (txfCRSDepTime.getText.isEmpty || txfDepTime.getText.isEmpty || txfArrivalTime.getText.isEmpty || txfTaxiOut.getText.isEmpty || txfDistance.getText.isEmpty || txfMonth.getText.isEmpty || txfDayOfWeek.getText.isEmpty)
            JOptionPane.showMessageDialog(null, "ERROR: Some data is missing!")
          else {
            try {
              //cogemos los datos de las variables de los campos del formulario
              val sd_time = txfCRSDepTime.getText.toDouble
              val d_time = txfDepTime.getText.toDouble
              val sa_time = txfArrivalTime.getText.toDouble
              val t_out = txfTaxiOut.getText.toDouble
              val distance = txfDistance.getText.toDouble
              val month = txfMonth.getText.toInt
              val d_week = txfDayOfWeek.getText.toInt

              //calculamos variables formateadas y variables restantes
              val sd_timeF = (sd_time - sd_time % 100) / 100 * 60 + (sd_time % 100)
              val d_timeF = (d_time - d_time % 100) / 100 * 60 + (d_time % 100)
              val d_delay = d_timeF - sd_timeF
              val sa_timeF = (sa_time - sa_time % 100) / 100 * 60 + (sa_time % 100)
              val se_time = sa_timeF - sd_timeF
              val a_delay = d_delay + t_out - 16

              //creamos el dataFrame con las variables que se usaran para la regresion
              val analysis = spark.createDataFrame(Seq(
                (a_delay, d_delay, t_out, month, se_time, d_week, distance, sd_timeF, d_timeF, sa_timeF)
              )).toDF("AirportDelay", "DepDelay", "TaxiOut", "Month", "CRSElapsedTime", "DayOfWeek", "Distance", "CRSDepTimeF", "DepTimeF", "CRSArrTimeF")

              val r = model.transform(analysis)
              val prediction = r.select("prediction").first.getDouble(0).toInt.toString

              //se muestra el resultado tanto por la IU como por consola
              txfArrivalDelay.setText(prediction)
              println("\nEstimated Arrival delay: " + prediction.toInt + " minutes")
            }
            catch {
              //excepciones capturadas por entrada incorrecta de datos que no sean numeros
              case nfe: NumberFormatException =>
                JOptionPane.showMessageDialog(null, "ERROR: Some data is incorrect!")
                println("Error: some data is incorret (" + nfe.getMessage + ")")
            }
          }
        }
      })

      btnClear.addActionListener(new ActionListener {
        override def actionPerformed(e: ActionEvent): Unit = {
          txfCRSDepTime.setText(null)
          txfDepTime.setText(null)
          txfArrivalTime.setText(null)
          txfTaxiOut.setText(null)
          txfDistance.setText(null)
          txfMonth.setText(null)
          txfDayOfWeek.setText(null)
          txfArrivalDelay.setText(null)
        }
      })

      frame.setVisible(true)

      //cuando se cierre la IU se acaba la sesion
      frame.addWindowListener(new WindowAdapter() {
        override def windowClosing(windowEvent: WindowEvent): Unit = {
          spark.stop
          println("Spark Session finished\n")
        }
      })

    } catch {
      case _: org.apache.hadoop.mapred.InvalidInputException =>
        println("ERROR: You must create the Model before using the Predictor.\n")
        spark.stop
        println("Spark Session finished\n")
      case e: Exception =>
        println("ERROR: Exception during program execution (+" + e.getMessage + ")")
        spark.stop
        println("Spark Session finished\n")
    }
  }
}
